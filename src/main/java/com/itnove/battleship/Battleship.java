package com.itnove.battleship;

public class Battleship {
    public static void main(String[] args) {
    }

    public static void initBoard(int[][] board) {
    }

    public static void showBoard(int[][] board) {
    }

    public static void initShips(int[][] ships) {
    }

    public static void shoot(int[] shoot) {
    }

    public static boolean hit(int[] shoot, int[][] ships) {
        return true;
    }

    public static void hint(int[] shoot, int[][] ships, int attempt) {
    }

    public static void changeboard(int[] shoot, int[][] ships, int[][] board) {
    }
}
